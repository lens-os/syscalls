#![no_std]
extern crate sc;

use core::ffi::c_void;
use sc::syscall;

#[repr(C)]
pub struct sockaddr {
    pub sa_family: u16,
    pub sa_data: [u8; 14],
}

#[repr(C)]
pub struct sockaddr_un {
    pub sun_family: u16,
    pub sun_path: [u8; 108],
}

#[repr(C)]
pub struct iovec {
    pub iov_base: *mut c_void,
    pub iov_len: usize,
}

#[repr(C)]
pub struct msghdr {
    pub msg_name: *mut c_void,
    pub msg_namelen: i32,
    pub msg_iov: *mut iovec,
    pub msg_iovlen: usize,
    pub msg_control: *mut c_void,
    pub msg_controllen: usize,
    pub msg_flags: u32,
}

#[repr(C)]
pub struct statx_timestamp {
    pub tv_sec: i64,
    pub tv_nsec: u32,
}

#[repr(C)]
pub struct statx_data {
    pub stx_mask: u32,
    pub stx_blksize: u32,
    pub stx_attributes: u64,
    pub stx_nlink: u32,
    pub stx_uid: u32,
    pub stx_gid: u32,
    pub stx_mode: u16,
    pub stx_ino: u64,
    pub stx_size: u64,
    pub stx_blocks: u64,
    pub stx_attributes_mask: u64,
    pub stx_atime: statx_timestamp,
    pub stx_btime: statx_timestamp,
    pub stx_ctime: statx_timestamp,
    pub stx_mtime: statx_timestamp,
    pub stx_rdev_major: u32,
    pub stx_rdev_minor: u32,
    pub stx_dev_major: u32,
    pub stx_dev_minor: u32,
}

pub fn read(fd: i32, buf: *mut u8, count: usize) -> usize {
    unsafe { syscall!(READ, fd, buf, count) }
}

pub fn write(fd: i32, buf: *const u8, count: usize) -> usize {
    unsafe { syscall!(WRITE, fd, buf, count) }
}

pub fn open(filename: *const u8, flags: i32, mode: u16) -> i32 {
    unsafe { syscall!(OPEN, filename, flags, mode) as i32 }
}

pub fn close(fd: i32) -> i32 {
    unsafe { syscall!(CLOSE, fd) as i32 }
}

pub fn ioctl(fd: i32, request: i32, argp: *mut c_void) -> i32 {
    unsafe { syscall!(IOCTL, fd, request, argp) as i32 }
}

pub fn statx(fd: i32, path: *const u8, flags: i32, mask: i32, statxbuf: *mut statx_data) -> i32 {
    unsafe { syscall!(STATX, fd, path, flags, mask, statxbuf) as i32 }
}

pub unsafe fn mmap(addr: u64, len: usize, prot: i32, flags: i32, fd: i32, off: usize) -> *mut u8 {
    syscall!(MMAP, addr, len, prot, flags, fd, off) as *mut u8
}

pub unsafe fn munmap(addr: *mut u8, size: usize) -> i32 {
    syscall!(MUNMAP, addr, size) as i32
}

pub fn socket(family: i32, socktype: i32, protocol: i32) -> i32 {
    unsafe { syscall!(SOCKET, family, socktype, protocol) as i32 }
}

pub fn connect(fd: i32, addr: *mut sockaddr, addrlen: usize) -> i32 {
    unsafe { syscall!(CONNECT, fd, addr, addrlen) as i32 }
}

pub fn accept(fd: i32, peer_addr: *mut sockaddr, peer_addrlen: usize) -> i32 {
    unsafe { syscall!(ACCEPT, fd, peer_addr, peer_addrlen) as i32 }
}

pub fn sendmsg(fd: i32, msg: *mut msghdr, flags: u32) -> i32 {
    unsafe { syscall!(SENDMSG, fd, msg, flags) as i32 }
}

pub fn recvmsg(fd: i32, msg: *mut msghdr, flags: u32) -> isize {
    unsafe { syscall!(RECVMSG, fd, msg, flags) as isize }
}

pub fn shutdown(fd: i32, how: i32) -> i32 {
    unsafe { syscall!(SHUTDOWN, fd, how) as i32 }
}

pub fn bind(fd: i32, myaddr: *mut sockaddr, addrlen: usize) -> i32 {
    unsafe { syscall!(BIND, fd, myaddr, addrlen) as i32 }
}

pub fn listen(fd: i32, backlog: i32) -> i32 {
    unsafe { syscall!(LISTEN, fd, backlog) as i32 }
}

pub fn getsockopt(fd: i32, level: i32, optname: i32, optval: *mut c_void, optlen: *mut c_void) -> i32 {
  unsafe { syscall!(GETSOCKOPT, fd, level, optname, optval, optlen) as i32 }
}

pub fn setsockopt(fd: i32, level: i32, optname: i32, optval: *mut c_void, optlen: usize) -> i32 {
  unsafe { syscall!(SETSOCKOPT, fd, level, optname, optval, optlen) as i32 }
}

pub fn fork() -> i32 {
    unsafe { syscall!(FORK) as i32 }
}

pub unsafe fn execve(filename: *const u8, argv: *const *const u8, envp: *const *const u8) -> i32 {
    syscall!(EXECVE, filename, argv, envp) as i32
}

pub fn exit(error_code: i32) -> ! {
    unsafe {
        syscall!(EXIT, error_code);
    }
    loop {} // ???
}

#[cfg(test)]
mod tests {
    #[test]
    fn write() {
        super::write(1, b"Hello, world!\n\0" as *const u8, 15); // Cargo test doesn't let us test for stdout.
    }
}
